import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';
import { Todo } from '../elements/todo.js';
import { CreateTodo } from '../elements/create-todo.js'
import { DeleteTodo } from '../elements/delete-todo.js'
/**
 * @customElement
 * @polymer
 */
class TodoReduxApp extends PolymerElement {
  static get template() {
    return html`
            <style>
                :host {
                    display: block;
                }
                
                .container {
                    display :flex;
                }
                
                .element {
                    border: 1px solid grey;
                    min-width: 40%;
                }
            </style>
            <div class="container">
                <div class="element">
                    <create-todo
                        todo=[[todo]]
                        on-add='add'
                    ></create-todo>
                    <todo-list
                        todo=[[todo]]
                    ></todo-list>
                </div>
                <div class="element">
                    <delete-todo
                        todo=[[todo]]
                        on-delete='delete'
                    ></delete-todo>
                </div>
            </div>
      
    `;
  }
  static get properties() {
    return {
      todo: {
        type: Array,
        value: [],
        notify: true,
      }
    };
  }
  add(e) {
    this.push('todo', e.detail);
  }

  delete(e) {
    var item = e.detail;
    var index = this.todo.indexOf(item);
    this.splice('todo', index, 1);
  }
}

window.customElements.define('todo-app', TodoReduxApp);
