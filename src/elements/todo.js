import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';
import '@polymer/polymer/lib/elements/dom-repeat.js';


/**
 * @customElement
 * @polymer
 */
export class Todo extends PolymerElement {

    static get is() {
        return 'todo-list';
    }
    static get template() {
        return html`
        <style>
            :host {
                display: block;
            }
        </style>
        <ul>
            <dom-repeat items="[[todo]]">
                <template>
                    <li>
                        [[item]]
                    </li>
                </template>
            </dom-repeat>
        </ul>
    `;
    }
    static get properties() {
        return {
            todo: {
                type: Array,
                reflectToAttribute: true,
            }
        };
    }
}
window.customElements.define(Todo.is, Todo);