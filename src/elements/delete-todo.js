import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/polymer/lib/elements/dom-repeat.js';



/**
 * @customElement
 * @polymer
 */
export class DeleteTodo extends PolymerElement {

  static get is() {
    return 'delete-todo';
  }
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <ul>
            <dom-repeat items="[[todo]]">
                <template>
                    <li>
                        [[item]] <button on-click='deleteToDo' data-item$=[[item]]>X</button>
                    </li>
                </template>
            </dom-repeat>
        </ul>
    `;
  }
  static get properties() {
    return {
      todo: {
        type: Array,
        reflectToAttribute: true,
        notify: true,
        observer: "filter",
      },
    };
  }

  deleteToDo(e) {
    this.dispatchEvent(new CustomEvent("delete", { detail: e.target.getAttribute("data-item"), bubbles: true }));
  }

  filter() {
    this.filteredTodo = this.todo;
    console.log(this.filteredTodo);
  }
}
window.customElements.define(DeleteTodo.is, DeleteTodo);