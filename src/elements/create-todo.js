import { html, PolymerElement } from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-input/paper-input.js';

/**
 * @customElement
 * @polymer
 */
export class CreateTodo extends PolymerElement {

  static get is() {
    return 'create-todo';
  }

  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <paper-input always-float-label id='todo' label='Floating label' value={{searchValue}}></paper-input>
      <button on-click='addToDo' id='addButton'>add</button>
      `;
  }

  addToDo() {
    this.dispatchEvent(new CustomEvent('add', { detail: this.$.todo.value, bubbles: true }));
  }

}
window.customElements.define(CreateTodo.is, CreateTodo);